package com.q3;
import java.util.ArrayList;

public class Array_List {
	public static void main(String[] args) {
		ArrayList<String> l = new ArrayList<>();
		l.add("Slice");
		l.add("Maazaa");
		l.add("Coke");
		l.add("Sprite");
		l.add("MountainDew");
		System.out.println(l.size());
		System.out.println(l);
		l.remove(1);
		if (l.contains("Coke")) {
			System.out.println("Coke is present in list");

		} else {
			System.out.println("Coke is not present in list");

		}

	}
}
