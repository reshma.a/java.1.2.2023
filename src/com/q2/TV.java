package com.q2;

public class TV implements TVremote{

	@Override
	public void smart() {
		System.out.println("This is smart method");
	}

	@Override
	public void remote() {
		System.out.println("This is remote method");
	}
public static void main(String[] args) {
	TV t= new TV();
	t.smart();
	t.remote();
}
}
