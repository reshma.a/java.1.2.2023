package com.q2;

public class SmartTelephone extends Telephone{

	@Override
	void lift() {
		System.out.println("This is Lift method");
	}

	@Override
	void disconnected() {
		System.out.println("This is disconnected method");
	}
public static void main(String[] args) {
	SmartTelephone s= new SmartTelephone();
	s.lift();
	s.disconnected();
}
}
